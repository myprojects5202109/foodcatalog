import React from 'react'
import { Link } from 'react-router-dom'

const CardItem = ({item}) => {
  return (
    <div >
        <div class="card h-100 mx-2  my-2 text-bg-danger " style={{width: '22rem'}}>
  <img src={item.thumbnail_url} class="card-img-top object-fit-cover " style={{height:'20vh'}} alt="..."/>
  <div class="card-body h-100">
    <h5 class="card-title">{item.name}</h5>
    <p class="card-text mb-4">{item.description===""? <span>No description available</span>: item.description.substring(0,100)}..</p>
    <Link to={'/item'} className='btn btn-primary' state={item} >Dive in</Link>
  </div>
</div>
    </div>
  )
}

export default CardItem