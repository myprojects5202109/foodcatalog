import React from 'react'
import { useDispatch } from 'react-redux'
import { useLocation } from 'react-router'
import { bindActionCreators } from 'redux'
import { actionCreators } from '../State'

const ItemDetails = () => {
     const dispatch=useDispatch()
     const {addtoCart}=bindActionCreators(actionCreators,dispatch)
   
    let {state}=useLocation()
   
    
  return (
    <div style={{backgroundImage:'url("https://videohive.img.customer.envatousercontent.com/files/b73b1f0c-062c-41dc-a6be-dc8778c82eed/inline_image_preview.jpg?auto=compress%2Cformat&fit=crop&crop=top&max-h=8000&max-w=590&s=2a9a3846083554d13618d5d5e7fbc5a4")' , height:'100vh', backgroundRepeat:'no-repeat', backgroundSize:'cover'}}>

<div className="card mb-3 bg-warning " style={{maxwidth: '540px'}}>
  <div className="row g-0">
    <div className="col-md-4">
      <img src={state.thumbnail_url} className="img-fluid rounded-start" alt="..." style={{height:'50vh'}}/>
      <button className='btn btn-success mx-3' data-bs-container="body" data-bs-toggle="popover" data-bs-placement="right" data-bs-content="Right popover" onClick={()=>{addtoCart(state); alert("Item added")}} >Add to Cart</button>
    </div>
    <div className="col-md-8">
      <div className="card-body">
        <h5 className="card-title fs-1">{state.name}</h5>
        <p className="card-text font-monospace">{state.description===""?<span>No description available</span>: state.description}</p>
        {
        state.instructions.map(item=><p className='fs-5'>{item.position}{'->'}{item.display_text}</p>)

        }
      </div>
    </div>
  </div>

</div>


    </div>
  )
}

export default ItemDetails