import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
const Navbar = () => {
  const cart=useSelector(state=>state.cart)
  
  
  return (
    <div>

<nav className="navbar bg-dark   ">
  <div className="container-fluid "  >
    <Link className="navbar-brand text-white" to='/'>
      <img src="/cheeseburger.png" alt="Logo" width="30" height="24" className="d-inline-block align-text-top mx-2"/>
 Tasty Point
    </Link>
    
    <Link to='/profile' className='btn btn-warning bi bi-person' state={cart}>Profile</Link>
    <Link className='btn btn-success bi bi-door-open' to='/login' >Login</Link>
  </div>
  
</nav>
    </div>
  )
}

export default Navbar