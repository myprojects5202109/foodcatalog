import React from 'react'
import { Link } from 'react-router-dom';
import { useLocation } from 'react-router'

const Profile = () => {
let {state}=useLocation();

    
  return (
    <div>
        <h1 className='text-center'>Saved Recipes</h1>
           <table className='table table-bordered'>
            <thead>
                <tr>
                    <th>S.no</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
{
        state.map(item=>
               <tr>
                <td>{state.indexOf(item)+1}</td>
                <td>{item.card.name}</td>
                <td><Link to={'/item'} className='btn btn-primary' state={item.card} >Dive in</Link></td>
               </tr>
        )
}
            </tbody>
           </table>

    </div>
  )
}

export default Profile