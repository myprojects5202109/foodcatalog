import React,{useEffect,useState} from 'react'
import FoodService from '../FoodService'
import CardItem from './CardItem'
import ReactPaginate from 'react-paginate';
const Home = () => {
    const [food, setFood] = useState([])
     const [loading, setloading] = useState(false)
     const [itemOffset, setItemOffset] = useState(0);
     const [itemsPerPage] = useState(6);
     const [Search, setSearch] = useState("")
   
    useEffect(() => {
      FoodService.getFood()
        .then(response => {
                    setFood(response.data)
                    console.log()
                    
                    
        })
        .catch(error => console.log(error));
      
    }, []);
    
   
    
    const endOffset = itemOffset + itemsPerPage;
    const currentItems = food.slice(itemOffset, endOffset);

    const pageCount = Math.ceil(food.length / itemsPerPage);
    const handlePageClick = (event) => {
        const newOffset = (event.selected * itemsPerPage) % food.length;
     console.log(currentItems)
        setItemOffset(newOffset);
    }

    




  return (
   <div style={{backgroundImage:'url("https://www.teahub.io/photos/full/255-2551960_wallpaper-hamburger-meat-french-fries-fast-food-fast.jpg")',backgroundSize:'cover' ,backgroundPosition:'center', height:'105vh', width:'100%' }}>
       <h1 className='fst-italic' style={{textAlign:'center',color:'whitesmoke'}}> Here You can view , search and save you recipe</h1>
       <div className="input-group " style={{paddingLeft:'120px', paddingRight:'120px'}} >
  <input type="search" className="form-control rounded" placeholder="Search by name,ingredients" onChange={(e)=>setSearch(e.target.value)} />
</div>
{ 
  loading? 
    <div className='text-center my-2'>
 <div class="spinner-grow text-warning " role="status">
 </div></div> :
 
 <div className='row row-cols-1 row-cols-md-3 g-4' >
{
currentItems.filter((val=>{return Search.toLowerCase()===""?val:(val.name.toLowerCase().includes(Search.toLowerCase))

}))

.map(item=>
           <div className='col' >
            <CardItem  item={item} />
           </div>
  )}
   </div>}
   <ReactPaginate
                breakLabel="..."
                nextLabel="next >"
                onPageChange={handlePageClick}
                pageRangeDisplayed={5}
                pageCount={pageCount}
                previousLabel="< previous"
                renderOnZeroPageCount={null}
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakClassName="page-item"
                breakLinkClassName="page-link"
                containerClassName="pagination justify-content-center"
                activeClassName="active"
            />

    </div>
  
  )}

export default Home