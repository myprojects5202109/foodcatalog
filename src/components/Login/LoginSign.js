import React,{Fragment, useState} from 'react'
import Login from './Login'
import Sign from './Sign'
import './style.css'
const LoginSign = () => {
    const [change, setchange] = useState(false)
  return (
    <div className='big' style={{backgroundImage:'url("https://b.rgbimg.com/users/b/ba/ba1969/600/nwkmcVK.jpg")'}}>
        <div className="btn-group ls text-center my-3" >
  <button type="button" className={`btl ${!change?'bg-info':''}`}    onClick={()=>setchange(false)}  >Login</button>
  <button type="button" className={`btl ${change?'bg-info':''}`}   onClick={()=>setchange(true)}>Sign in</button>
</div>
 <Fragment>
    {
    change? <Sign/>:<Login/>
        }
        </Fragment>
    </div>
  )
}

export default LoginSign