import React from 'react'
import './style.css'
const Login = () => {
  return (
    <div className='whole' >
<form className='base card mx-auto'  >
  <img src='/login.gif' alt='login/signin' className='img-fluid rounded'  style={{height:'15vh'}}  />
  <div className="my-2">
    <label  className="form-label mx-2 fs-5 fw-semibold">Username</label>
    <input type="email" className=" size " id="exampleInputEmail1" aria-describedby="emailHelp"/>
    <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
  </div>
  <div className="mb-3">
    <label  className="form-label mx-2 fs-5 fw-semibold ">Password</label>
    <input type="password" className=" size" id="exampleInputPassword1"/>
  </div>
  
  <button type="submit" className="btn btn-danger ">Login</button>
</form>
    </div>
  )
}

export default Login