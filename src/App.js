
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './App.css';
import Navbar from './components/Navbar';
import Home from './components/Home';
import ItemDetails from './components/ItemDetails';
import Profile from './components/Profile';
import LoginSign from './components/Login/LoginSign';

function App() {
  return (
    <div >
       <BrowserRouter>
      <Navbar/>
      <Routes>
      <Route path='/'  element={ <Home/>}/>
      <Route path='/item'  element={<ItemDetails/>} />
      <Route path='/profile'  element={<Profile/>} />
      <Route  path='/login'  element={<LoginSign/>} />
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
