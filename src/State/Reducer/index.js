import { combineReducers } from "redux";
import CartReducer from './CartReducer'

const Reducers=combineReducers({
    cart:CartReducer
})
export default Reducers